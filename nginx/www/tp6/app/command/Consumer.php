<?php
declare (strict_types = 1);

namespace app\command;
use app\controller\Consumer as MqConsumer;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class Consumer extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('consumer')
            ->setDescription('the consumer command');
	
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        //$output->writeln('consumer');
		$consumer = new MqConsumer();
        $consumer->start();//调用消费者
    }
}
