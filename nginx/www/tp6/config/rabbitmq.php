<?php
    return [

        # 连接信息
        'AMQP' => [
            #'host' => '127.0.0.1',
            'host' => '172.18.0.2',
            'port'=>'5672',
            'login'=>'guest',
            'password'=>'guest',
			'connection_timeout' => 10,
            'vhost'=>'/'
        ],
        # 邮件队列
        'email_queue' => [
            'exchange_name' => 'email_exchange',
            'exchange_type'=>'direct',#直连模式
            'queue_name' => 'email_queue',
            'route_key' => 'email_roteking',
            'consumer_tag' => 'consumer'
        ],
		# 商品队列【fanout】
		'goods_queue' => [
			'exchange_name' => 'fanout_exchange',
			'exchange_type'=>'fanout',#扇形
			'queue_name' => 'goods_queue',
			'route_key' => 'goods_routerking',
			'consumer_tag' => 'goods'
		],
		# 订单队列【fanout】
		'order_queue' => [
			'exchange_name' => 'fanout_exchange',
			'exchange_type'=>'fanout',#扇形
			'queue_name' => 'order_queue',
			'route_key' => 'order_routerking',
			'consumer_tag' => 'order'
		],
		# fanout队列
		'fanout_queue' => [
			'exchange_name' => 'fanout_exchange',
			'exchange_type'=>'fanout',#扇形
			'queue_name' => 'fanout_queue',
			'route_key' => 'fanout_routerking',
			'consumer_tag' => 'fanout'
		],
		 # 商品队列
		'goods_topic_queue' => [
			'exchange_name' => 'topic_exchange',    //对应topic队列交换机名称
			'exchange_type'=>'topic',           //主题
			'queue_name' => 'goods_queue', 
			'route_key' => "*.goods",      //模糊匹配
			'consumer_tag' => 'goods'
		],
		# 订单队列
		'order_topic_queue' => [
			'exchange_name' => 'topic_exchange',//对应topic队列交换机名称
			'exchange_type'=>'topic',//主题
			'queue_name' => 'order_queue',
			'route_key' => '*.order',    //模糊匹配
			'consumer_tag' => 'order'
		],
		# topic队列
		'topic_queue' => [
			'exchange_name' => 'topic_exchange',
			'exchange_type'=>'topic',#主题
			'queue_name' => 'topic_queue',
			#'route_key' => "topic.goods.order",
			'route_key' => "topic.order",
			'consumer_tag' => 'topic'
		],



];
